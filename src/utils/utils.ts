import axios from 'axios'
import qs from "qs"
const gatewayUrl = import.meta.env.VITE_APP_BASEURL


axios.interceptors.request.use(config => {
    return config
  }, error => {
    return Promise.reject(error);
  });
  axios.interceptors.response.use(response => {
    return response
  }, error => {
    return Promise.resolve(error.response)
});

function checkStatus(response:any):any {
    // loading
    // 如果http状态码正常，则直接返回数据
    if (response && (response.status === 200 || response.status === 304 || response.status === 400 ||  response.status === 401)) {
      if(response.data.code == 1000000009 || response.data.code == 1000000010){
        localStorage.clear();
        sessionStorage.clear();
        window.location.href = '/xiaoyunxingAdmin/login';
        return
    }
      return response.data;
      // 如果不需要除了data之外的数据，可以直接 return response.data
    }
    // 异常状态下，把错误信息返回去
    return {
      status: -404,
      msg: '系统出小差了,请稍后再试~'
    }
  }
  function checkCode(res:any):any {
    // 如果code异常(这里已经包括网络错误，服务器错误，后端抛出的错误)，可以弹出一个错误提示，告诉用户
    if (res.status === -404) {
      console.log('404', '网络异常')
    }
    if (res.data && (!res.data.success)) {
  
    }
    return res
  }
  function postFormby(url:string, data:object, header?:object){
    let _url = '';
    if (url.indexOf('http://') != -1 || url.indexOf('https://') != -1) {
      _url = url
    } else {
      _url = gatewayUrl + url
    }
    const headers = header || {};
    return axios({
      method: 'post',
      url: _url,
      data: qs.stringify(data),
      timeout: 3000,
      headers: Object.assign({
        'X-Requested-With': 'XMLHttpRequest',
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
      }, headers)
    }).then(
      (response) => {
        return checkStatus(response)
      }
    ).then(
      (res) => {
        return checkCode(res)
      }
    )
  }
  function postJsonby(url:string, data:object, header?:object){
    let _url = '';
    if (url.indexOf('http://') != -1 || url.indexOf('https://') != -1) {
      _url = url
    } else {
      _url = gatewayUrl + url
    }
    let headers = header || {};
    if(localStorage.getItem("userInfo")){
      headers = {
        token:JSON.parse(localStorage.getItem("userInfo") as string).token
      }
    }
    return axios({
      method: 'post',
      url: _url,
      data: data,
      timeout: 3000,
      headers: Object.assign({
        'X-Requested-With': 'XMLHttpRequest',
        'Content-Type': 'application/json; charset=UTF-8'
      }, headers)
    }).then(
      (response) => {
        return checkStatus(response)
      }
    ).then(
      (res) => {
        return checkCode(res)
      }
    )
  }
  function getby(url:string, params:object, header?:object){
    const headers = header || {}
    let _url = '';
    if (url.indexOf('http://') != -1 || url.indexOf('https://') != -1) {
      _url = url
    } else {
      _url = gatewayUrl + url
    }
    return axios({
      method: 'get',
      url: _url,
      params, // get 请求时带的参数
      timeout: 3000,
      headers: Object.assign({
        'X-Requested-With': 'XMLHttpRequest'
      }, headers)
    }).then(
      (response) => {
        return checkStatus(response)
      }
    ).then(
      (res) => {
        return checkCode(res)
      }
    )
  }
  function combineFromParamForNullBy(param:{[key: string]: {x: number | string}}){ //组合参数
    const params:{[key: string]: {x: number | string}} = {};
    for (var i in param) {
      if (param[i] != null) {
        params[i] = param[i]
      }
    }
    return params
  }

export const gatewaybyUrl = gatewayUrl
export const postForm = (url: string,data: object,header?: object) => postFormby(url,data,header)
export const postJson = (url: string,data: object,header?: object) => postJsonby(url,data,header)
export const get = (url: string,data: object,header?: object) => getby(url,data,header)
export const combineFromParamForNull = (param:object) => combineFromParamForNullBy(param as {[key: string]: {x: number | string}})
