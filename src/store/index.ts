import {createStore} from 'vuex'
import {postJson,get} from '@/utils/utils'
import { ElMessage } from "element-plus";
import OSS from "ali-oss"
var myTimeOss:any = 0
var time = 1000000
interface labvalue {
    path? : any,
    name?:any,
    title?:any
}
export default createStore({
    state: {
        tagsList:[],
        collapse: false,
        ossclient:{},
        configdata:[{confAlias:'' as string,confNum:'' as string}],
        imageTitle:"",
        H5url:"",
    },
    mutations: {
        delTagsItem(state, data) {
            state
                .tagsList
                .splice(data.index, 1);
        },
        setTagsItem(state, data: {name: string, title: string, path:string}) {
            state
                .tagsList
                .push(data as never)
        },
        clearTags(state) {
            state.tagsList = []
        },
        closeTagsOther(state, data) {
            state.tagsList = data;
        },
        closeCurrentTag(state, data) {
            for (let i = 0, len = state.tagsList.length; i < len; i++) {
                const item:labvalue = state.tagsList[i];
                if (item.path === data.$route.fullPath) {
                    if (i < len - 1) {
                        data
                            .$router
                            .push(state.tagsList[i + 1] as labvalue['path']);
                    } else if (i > 0) {
                        data
                            .$router
                            .push(state.tagsList[i - 1] as labvalue['path']);
                    } else {
                        data
                            .$router
                            .push("/");
                    }
                    state
                        .tagsList
                        .splice(i, 1);
                    break;
                }
            }
        },
        // 侧边栏折叠
        handleCollapse(state, data) {
            state.collapse = data;
        },
        setossclient(state, data){
            state.ossclient = new OSS({
                // yourRegion填写Bucket所在地域。以华东1（杭州）为例，Region填写为oss-cn-hangzhou。
                region: 'oss-cn-shanghai',
                // 从STS服务获取的临时访问凭证。临时访问凭证包括临时访问密钥（AccessKeyId和AccessKeySecret）和安全令牌（SecurityToken）。
                accessKeyId: data.credentials.accessKeyId,
                accessKeySecret: data.credentials.accessKeySecret,
                stsToken: data.credentials.securityToken,
                // 填写Bucket名称。
                bucket: this.state.configdata.find((i: { confAlias: string; })=>{return i.confAlias=='BUCKET_NAME'}).confNum
            });
        },
        setcinfigdata(state, data){
            state.imageTitle = data.find((i: { confAlias: string; })=>{return i.confAlias=='DNS_URl'}).confNum
            state.H5url = data.find((i: { confAlias: string; })=>{return i.confAlias=='H5_URL'}).confNum
            state.configdata = data
        },
        
    },
    actions: {
        getconfigdata({ commit }){
            let that = this
        if(JSON.parse(localStorage.getItem('userInfo') as string)){
            return new Promise<void>((resolve, reject) => {
                postJson(`/pailiying-user/v1/get/confNumByConfAlias`,
          {
            uid:JSON.parse(localStorage.getItem("userInfo") as string).uid       
        }).then((res)=> {
                        if (res.code ===  0) {
                        commit('setcinfigdata',res.data)
                        that.dispatch("getossclient");
                        resolve()
                        } else {
                            ElMessage.error(res.message)
                        }
                    }) 
              })
            }
           
        },
        getossclient({ commit }){
            let that = this
            clearTimeout(myTimeOss)
            if(JSON.parse(localStorage.getItem('userInfo') as string)){
            return new Promise<void>((resolve, reject) => {
                get(`/pailiying-user/file/aliyun/sts`,
          {
            uid:JSON.parse(localStorage.getItem("userInfo") as string).uid       
        }).then((res)=> {
                        if (res.code ===  0) {
                            myTimeOss = setTimeout(() => {
                            that.dispatch("getossclient");
                           },time);
                           commit('setossclient',res.data)
                            resolve()
                        } else {
                            ElMessage.error(res.message)
                        }
                    }) 
              })
            }
           
        }
    },
    modules: {}
})