import vue from '@vitejs/plugin-vue'
import path from "path"
import { ConfigEnv, UserConfig,loadEnv } from 'vite'
const pathResolve = (pathStr: string) => {
    return path.resolve(__dirname, pathStr);
}
//可异步请求配置
// function asyncFunctions(mode){
//   return new Promise((reslove, reject)=>{
//     setTimeout(() => {
//       reslove(parseInt(loadEnv(mode, process.cwd()).VITE_APP_PROD))
//     }, 5000);
//   })
// }
export default async ({mode }: ConfigEnv): Promise<UserConfig> =>{
//  let aa = await asyncFunctions(mode)
  return { 
    base: './',
    server: {				
      host: true,
      port:parseInt(loadEnv(mode, process.cwd()).VITE_APP_PROD)
    },
    resolve:{
      alias: {
        '@': pathResolve('./src'),
      },
    },
    build: {
      target: 'es2015',
      outDir: 'vite-init',
      terserOptions: {
        compress: {
          keep_infinity: true,
          drop_console: false,//是都删除console.log
        },
      },
      brotliSize: false,
      chunkSizeWarningLimit: 2000,
    },
      // outDir: 'vite-init',//构建输出将放在其中。会在构建之前删除旧目录。@default 'dist'
      // minify: 'esbuild',//构建时的压缩方式
      optimizeDeps: {// 引入第三方的配置
        include: ['axios','schart.js']
      },
      // pluginOptions: {
      //   'style-resources-loader': {
      //     preProcessor: 'scss',
      //     patterns: []
      //   }
      // },
      // proxy: {//代理配置
      //   '/api': {
      //     target: 'http://xx.xx.xx.xx:xxxx',
      //     changeOrigin: true,
      //     ws: true,
      //     rewrite: (path: string) => { path.replace(/^\/api/, '') }
      //   }
      // }
    plugins: [vue()],
    // pages: {
    //     index: {
    //         entry: 'src/main.ts'
    //     }
    // }
  }
}